package dominio;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;


@Getter
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class Libro {

	private final String isbn;
	private final String titulo;
	private final int anio;

}
