package dominio;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.util.Date;

@Getter
@RequiredArgsConstructor
@ToString
@EqualsAndHashCode
public class Prestamo {

    private final Date fechaSolicitud;
    private final Libro libro;
    private final Date fechaEntregaMaxima;
    private final String nombreUsuario;


}
