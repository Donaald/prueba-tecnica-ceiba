package dominio;

import dominio.excepcion.PrestamoException;
import dominio.repositorio.RepositorioLibro;
import dominio.repositorio.RepositorioPrestamo;


import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Objects;

public class Bibliotecario {

    public static final String EL_LIBRO_NO_SE_ENCUENTRA_DISPONIBLE = "El libro no se encuentra disponible";
    public static final String EL_ISBN_ES_PALINDORMO = "los libros palíndromos solo se pueden utilizar en la biblioteca";

    private RepositorioLibro repositorioLibro;
    private RepositorioPrestamo repositorioPrestamo;

    public Bibliotecario(RepositorioLibro repositorioLibro, RepositorioPrestamo repositorioPrestamo) {
        this.repositorioLibro = repositorioLibro;
        this.repositorioPrestamo = repositorioPrestamo;

    }

    public void prestar(String isbn, String nombreUsuario) {

        if (this.esPrestado(isbn)) {
            throw new PrestamoException(EL_LIBRO_NO_SE_ENCUENTRA_DISPONIBLE);
        }

        if (esPalindromo(isbn)) {
            throw new PrestamoException(EL_ISBN_ES_PALINDORMO);
        }


        Libro libro = this.repositorioLibro.obtenerPorIsbn(isbn);
        Prestamo prestamo = new Prestamo(new Date(), libro, calcularFecha(isbn), nombreUsuario);
        this.repositorioPrestamo.agregar(prestamo);

    }

    public boolean esPrestado(String isbn) {
        Libro libroPrestado = this.repositorioPrestamo.obtenerLibroPrestadoPorIsbn(isbn);
        return !Objects.isNull(libroPrestado);
    }

    public Boolean esPalindromo(String s) {
        int N = s.length();
        for (int i = 0; i < N / 2; i++)
            if (s.charAt(i) != s.charAt(N - 1 - i))
                return false;
        return true;
    }

    private boolean numerosSumanMasDeTreinta(String isbn) {
        int acumulado = 0;
        char[] charNumeros = isbn.toCharArray();
        for (int i = 0; i < isbn.length(); i++) {
            if (Character.isDigit(charNumeros[i])) {
                acumulado += Character.getNumericValue(charNumeros[i]);
            }
        }
        return acumulado > 30;
    }

    private Date calcularFecha(String isbn) {

        boolean bandera = true;
        int acumulado = 1;
        LocalDate localDate = LocalDate.now();

        if (this.numerosSumanMasDeTreinta(isbn)) {

            while (bandera) {
                DayOfWeek dayOfWeek = localDate.getDayOfWeek();
                if (!dayOfWeek.equals(DayOfWeek.SUNDAY)) {
                    acumulado++;
                }

                localDate = localDate.plusDays(1);

                if (acumulado == 15) {
                    bandera = false;
                }
            }

            return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());

        }

        return null;

    }

}
