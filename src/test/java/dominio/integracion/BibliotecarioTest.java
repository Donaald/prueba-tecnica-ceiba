package dominio.integracion;

import static org.junit.Assert.fail;

import dominio.Prestamo;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import dominio.Bibliotecario;
import dominio.Libro;
import dominio.excepcion.PrestamoException;
import dominio.repositorio.RepositorioLibro;
import dominio.repositorio.RepositorioPrestamo;
import persistencia.sistema.SistemaDePersistencia;
import testdatabuilder.LibroTestDataBuilder;

import java.util.Date;

public class BibliotecarioTest {

    private static final String CRONICA_DE_UNA_MUERTA_ANUNCIADA = "Cronica de una muerta anunciada";

    private SistemaDePersistencia sistemaPersistencia;

    private RepositorioLibro repositorioLibros;
    private RepositorioPrestamo repositorioPrestamo;

    @Before
    public void setUp() {

        sistemaPersistencia = new SistemaDePersistencia();

        repositorioLibros = sistemaPersistencia.obtenerRepositorioLibros();
        repositorioPrestamo = sistemaPersistencia.obtenerRepositorioPrestamos();

        sistemaPersistencia.iniciar();
    }


    @After
    public void tearDown() {
        sistemaPersistencia.terminar();
    }

    @Test
    public void prestarLibroTest() {

        // arrange
        Libro libro = new LibroTestDataBuilder().conTitulo(CRONICA_DE_UNA_MUERTA_ANUNCIADA).build();
        repositorioLibros.agregar(libro);
        Bibliotecario blibliotecario = new Bibliotecario(repositorioLibros, repositorioPrestamo);

        // act
        blibliotecario.prestar(libro.getIsbn(), "NombreUsuario");

        // assert
        Assert.assertTrue(blibliotecario.esPrestado(libro.getIsbn()));
        Assert.assertNotNull(repositorioPrestamo.obtenerLibroPrestadoPorIsbn(libro.getIsbn()));

    }

    @Test
    public void prestarLibroNoDisponibleTest() {

        // arrange
        Libro libro = new LibroTestDataBuilder().conTitulo(CRONICA_DE_UNA_MUERTA_ANUNCIADA).build();

        repositorioLibros.agregar(libro);

        Bibliotecario blibliotecario = new Bibliotecario(repositorioLibros, repositorioPrestamo);

        // act
        blibliotecario.prestar(libro.getIsbn(), "NombreUsuario");
        try {

            blibliotecario.prestar(libro.getIsbn(), "NombreUsuario");
            fail();

        } catch (PrestamoException e) {
            // assert
            Assert.assertEquals(Bibliotecario.EL_LIBRO_NO_SE_ENCUENTRA_DISPONIBLE, e.getMessage());
        }
    }

    @Test
    public void isbPalindromo() {
        // arrange
        Libro libro = new LibroTestDataBuilder().conTitulo(CRONICA_DE_UNA_MUERTA_ANUNCIADA).conIsbn("12344321").build();

        repositorioLibros.agregar(libro);

        Bibliotecario blibliotecario = new Bibliotecario(repositorioLibros, repositorioPrestamo);

        try {

            blibliotecario.prestar(libro.getIsbn(), "NombreUsuario");
            fail();

        } catch (PrestamoException e) {
            // assert
            Assert.assertEquals(Bibliotecario.EL_ISBN_ES_PALINDORMO, e.getMessage());
        }
    }

    @Test
    public void isbNoPalindromo() {
        // arrange
        Libro libro = new LibroTestDataBuilder().conTitulo(CRONICA_DE_UNA_MUERTA_ANUNCIADA).conIsbn("123").build();
        repositorioLibros.agregar(libro);
        Bibliotecario blibliotecario = new Bibliotecario(repositorioLibros, repositorioPrestamo);

        // act
        blibliotecario.prestar(libro.getIsbn(), "NombreUsuario");

        // assert
        Assert.assertTrue(blibliotecario.esPrestado(libro.getIsbn()));
        Assert.assertFalse(blibliotecario.esPalindromo(libro.getIsbn()));
        Assert.assertNotNull(repositorioPrestamo.obtenerLibroPrestadoPorIsbn(libro.getIsbn()));
    }

    @Test
    public void verificarPrestamo() {
        // arrange
        Libro libro = new LibroTestDataBuilder().conTitulo(CRONICA_DE_UNA_MUERTA_ANUNCIADA).conIsbn("123").conAnio(1992).build();
        repositorioLibros.agregar(libro);
        Bibliotecario blibliotecario = new Bibliotecario(repositorioLibros, repositorioPrestamo);

        Prestamo prestamoEsperado = new Prestamo(new Date(), libro, null, "NombreUsuario");

        // act
        blibliotecario.prestar(libro.getIsbn(), "NombreUsuario");
        Prestamo pretamoBD = repositorioPrestamo.obtener(libro.getIsbn());
        Assert.assertEquals(prestamoEsperado.toString(), pretamoBD.toString());

    }


}
