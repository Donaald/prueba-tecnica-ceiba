package dominio.unitaria;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import dominio.excepcion.PrestamoException;
import org.junit.Assert;
import org.junit.Test;

import dominio.Bibliotecario;
import dominio.Libro;
import dominio.repositorio.RepositorioLibro;
import dominio.repositorio.RepositorioPrestamo;
import testdatabuilder.LibroTestDataBuilder;

public class BibliotecarioTest {

    @Test
    public void esPrestadoTest() {

        // arrange
        LibroTestDataBuilder libroTestDataBuilder = new LibroTestDataBuilder();

        Libro libro = libroTestDataBuilder.build();

        RepositorioPrestamo repositorioPrestamo = mock(RepositorioPrestamo.class);
        RepositorioLibro repositorioLibro = mock(RepositorioLibro.class);

        when(repositorioPrestamo.obtenerLibroPrestadoPorIsbn(libro.getIsbn())).thenReturn(libro);

        Bibliotecario bibliotecario = new Bibliotecario(repositorioLibro, repositorioPrestamo);

        // act
        boolean esPrestado = bibliotecario.esPrestado(libro.getIsbn());

        //assert
        assertTrue(esPrestado);
    }

    @Test
    public void libroNoPrestadoTest() {

        // arrange
        LibroTestDataBuilder libroTestDataBuilder = new LibroTestDataBuilder();

        Libro libro = libroTestDataBuilder.build();

        RepositorioPrestamo repositorioPrestamo = mock(RepositorioPrestamo.class);
        RepositorioLibro repositorioLibro = mock(RepositorioLibro.class);

        when(repositorioPrestamo.obtenerLibroPrestadoPorIsbn(libro.getIsbn())).thenReturn(null);

        Bibliotecario bibliotecario = new Bibliotecario(repositorioLibro, repositorioPrestamo);

        // act
        boolean esPrestado = bibliotecario.esPrestado(libro.getIsbn());

        //assert
        assertFalse(esPrestado);
    }

    @Test
    public void verificarLibroPrestadoMasDeUnaVez() {

        LibroTestDataBuilder libroTestDataBuilder = new LibroTestDataBuilder();

        Libro libro = libroTestDataBuilder.build();

        RepositorioPrestamo repositorioPrestamo = mock(RepositorioPrestamo.class);
        RepositorioLibro repositorioLibro = mock(RepositorioLibro.class);

        when(repositorioPrestamo.obtenerLibroPrestadoPorIsbn(libro.getIsbn())).thenReturn(libro);

        Bibliotecario bibliotecario = new Bibliotecario(repositorioLibro, repositorioPrestamo);

        try {

            bibliotecario.prestar(libro.getIsbn(), "NombreUsuario");
            fail();

        } catch (PrestamoException e) {
            Assert.assertEquals(Bibliotecario.EL_LIBRO_NO_SE_ENCUENTRA_DISPONIBLE, e.getMessage());
        }

    }


    @Test
    public void isbPalindromo() {

        Libro libro = new Libro("12344321", "nombre", 1992);

        RepositorioPrestamo repositorioPrestamo = mock(RepositorioPrestamo.class);
        RepositorioLibro repositorioLibro = mock(RepositorioLibro.class);

        when(repositorioPrestamo.obtenerLibroPrestadoPorIsbn(libro.getIsbn())).thenReturn(null);

        Bibliotecario bibliotecario = new Bibliotecario(repositorioLibro, repositorioPrestamo);

        try {

            bibliotecario.prestar(libro.getIsbn(), "NombreUsuario");
            fail();

        } catch (PrestamoException e) {
            Assert.assertEquals(Bibliotecario.EL_ISBN_ES_PALINDORMO, e.getMessage());
        }


    }

    @Test
    public void calculoDeFechaMximaPrestamo() {

        Libro libro = new Libro("A874B69Q", "El coronel no tiene quien le escriba", 1961);

        RepositorioPrestamo repositorioPrestamo = mock(RepositorioPrestamo.class);
        RepositorioLibro repositorioLibro = mock(RepositorioLibro.class);

        when(repositorioPrestamo.obtenerLibroPrestadoPorIsbn(libro.getIsbn())).thenReturn(null);

        Bibliotecario bibliotecario = new Bibliotecario(repositorioLibro, repositorioPrestamo);

        bibliotecario.prestar(libro.getIsbn(), "nombreUsuario");


    }


}
